﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace BTVN
{
    internal class Program
    {
        static void Main(string[] args)
        {



            int choice;
            do
            {

                //Viết 1 chương trình nhập vào 2 số 
                //  Viết các hàm tính toán + - * /

                Console.WriteLine("Chon mot trong cac bai duoi day:");
                Console.WriteLine("1. Tim do dai cua chuoi duoc nhap.");
                Console.WriteLine("2. Dem so tu trong chuoi.");
                Console.WriteLine("3. Dem so nguyen am va phu am trong chuoi.");
                Console.WriteLine("4. Viet chuong trinh C# tính tong cac chu so cau mot so nguyen n.");
                Console.WriteLine("5. Sap xep mang theo thu tu giam dan va in ra man hinh.");
                Console.WriteLine("6. Nhap 1 so , tính giai thua cua so do va xuat ra man hinh.");
                Console.WriteLine("7. Nhap va xuat ra man hinh thong tin can cuoc cua 1 cong dan Viet Nam.");
                Console.WriteLine("8. Nhap so nguyen duong n va tim n so Fibonacci");
                Console.WriteLine("9. Viet chuong trinh nhap vao 2 so phep +");
                Console.WriteLine("0. Thoat chương trình.");

                void Runtime()
                {
                    Console.Write("Nhap tuy chon cua ban: ");
                    choice = Convert.ToInt32(Console.ReadLine());

                    switch (choice)
                    {
                        case 1:
                            Console.WriteLine("Hay nhap chuoi:");
                            string str = Console.ReadLine();
                            int chuoi = 0;
                            foreach (char c in str)
                            {
                                chuoi++;
                            }
                            Console.WriteLine("Do dai cua chuoi la: " + chuoi);
                            Runtime();
                            break;

                        case 2:
                            Console.WriteLine("Hay nhap chuoi:");
                            string str2 = Console.ReadLine();
                            int chuoi2 = 0;
                            foreach (string word in str2.Split(' '))
                            {
                                chuoi2++;
                            }
                            Console.WriteLine("So tu trong chuoi la: " + chuoi2);
                            Runtime();
                            goto case 1;
                            break;

                        case 3:

                            Console.WriteLine("Hay nhap chuoi:");
                            string str3 = Console.ReadLine();
                            int nguyenam = 0;
                            int phuam = 0;
                            foreach (char c in str3.ToLower())
                            {
                                if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u') // nguyen am
                                {
                                    nguyenam++;
                                }
                                else if (char.IsLetter(c))
                                {
                                    phuam++;
                                }
                            }
                            Console.WriteLine("So nguyen am : " + nguyenam);
                            Console.WriteLine("So phu am : " + phuam);
                            Runtime();
                            break;

                        case 4:
                            Console.WriteLine("Hay nhap so: ");
                            int num = int.Parse(Console.ReadLine());
                            int sum = 0;
                            while (num > 0)
                            {
                                sum += num % 10;
                                num /= 10;
                            }
                            Console.WriteLine("Tong: " + sum);
                            Runtime();
                            break;

                        case 5:
                            Console.WriteLine("Hay nhap kich thuoc mang :");
                            int size = int.Parse(Console.ReadLine());
                            int[] arr = new int[size];
                            Console.WriteLine("Nhap cac so trong mang :");
                            for (int i = 0; i < size; i++)
                            {
                                arr[i] = int.Parse(Console.ReadLine());
                            }
                            Array.Sort(arr);
                            Array.Reverse(arr);
                            Console.WriteLine("Mang duoc sap xep theo thu tu giam dan :");
                            foreach (int element in arr)
                            {
                                Console.Write(element + " ");
                            }
                            Console.WriteLine();
                            Runtime();
                            break;

                        case 6:
                            Console.WriteLine("Hay nhap so:");
                            int num2 = int.Parse(Console.ReadLine());
                            int factorial = 1;
                            for (int i = 1; i <= num2; i++)
                            {
                                factorial *= i;
                            }
                            Console.WriteLine("Giai thua cua:  " + num2 + " = " + factorial);
                            Runtime();
                            break;

                        case 7:
                            CitizenInfo citizen = new CitizenInfo();
                            Console.WriteLine("Nhap thong tin cong dan:");
                            Console.WriteLine("Ho va ten:");
                            citizen.Name = Console.ReadLine();
                            Console.WriteLine(" Ngay/thang/nam :");
                            citizen.DateOfBirth = Console.ReadLine();
                            Console.WriteLine("Noi Sinh:");
                            citizen.PlaceOfBirth = Console.ReadLine();
                            Console.WriteLine("CCCD :");
                            citizen.CardNumber = Console.ReadLine();
                            Console.WriteLine("Dia Chi:");
                            citizen.Address = Console.ReadLine();

                            Console.WriteLine("Thong tin cua cong dan:");
                            Console.WriteLine("Ten la: " + citizen.Name);
                            Console.WriteLine("Nam Sinh la: " + citizen.DateOfBirth);
                            Console.WriteLine("Noi Sinh la: " + citizen.PlaceOfBirth);
                            Console.WriteLine("CCCD la: " + citizen.CardNumber);
                            Console.WriteLine("Dia Chi la: " + citizen.Address);
                            Runtime();
                            break;

                        case 8:
                            Console.WriteLine("Nhap so nguyen duong:");
                            int n = int.Parse(Console.ReadLine());
                            int a = 0;
                            int b = 1;
                            Console.Write("N so Fibonacci:");
                            for (int i = 0; i < n; i++)
                            {
                                Console.Write(a + " ");
                                int c = a + b;
                                a = b;
                                b = c;
                            }
                            Console.WriteLine();
                            Runtime();
                            break;
                        case 9:
                            double n1, n2, kq;
                            char op;
                            Console.WriteLine("Hay nhap so dau tien :");
                            n1 = Convert.ToDouble(Console.ReadLine());
                            Console.WriteLine("Hay nhap so thu hai : ");
                            n2 = Convert.ToDouble(Console.ReadLine());
                            Console.WriteLine("Nhap phep tinh ban muon thuc hien: ");
                            op = Convert.ToChar(Console.ReadLine());
                            if (op == '+')
                            {
                                kq = n1 + n2;
                                Console.WriteLine("{0} + {1} = {2}", n1, n2, kq);
                            }
                            else if (op == '-')
                            {
                                kq = n1 - n2;
                                Console.WriteLine("{0} - {1} = {2}", n1, n2, kq);
                            }
                            else if (op == '*')
                            {
                                kq = n1 * n2;
                                Console.WriteLine("{0} * {1} = {2}", n1, n2, kq);
                            }
                            else if (op == '/')
                            {
                                kq = n1 / n2;
                                Console.WriteLine("{0} / {1} = {2}", n1, n2, kq);
                            }

                            else
                            {
                                Console.WriteLine("Khong thuc hien đuoc");
                            }
                            Console.ReadLine();
                            Runtime();

                            break;

                        case 0:
                            Console.WriteLine("Roi khoi chuong trinh...");
                            break;
                        case '.':
                            Console.WriteLine("Khong nhap gi ca ? Roi khoi");
                            break;

                        default:
                            Console.WriteLine("lua chon sai , hay thu lai.");
                            Runtime();
                            break;
                    }
                }
                Runtime();
            } while (choice != 0);
        }

        public struct CitizenInfo
        {
            public string Name;
            public string DateOfBirth;
            public string PlaceOfBirth;
            public string CardNumber;
            public string Address;
        }
    }
}

