﻿using Day_3.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day_3
{
    internal class Choose
    {
        //1. Xuất dữ liệu Post, Category
        public void GetAll()
        {
            using var dbcontext = new MyDBContext();
            var posts = dbcontext.posts.ToList();
            var categories = dbcontext.categories.ToList();
            Console.WriteLine("=============Category==========");
            foreach (var category in categories)
            {
                Console.WriteLine($"Category Id : {category.Id}");
                Console.WriteLine($"Category Name: {category.Name}");
                Console.WriteLine();
            }
            Console.WriteLine("=============Post==========");
            foreach (var post in posts)
            {
                Console.WriteLine($"Id: {post.Id}");
                Console.WriteLine($"Title: {post.Title}");
                Console.WriteLine($"Author: {post.Author}");
                Console.WriteLine($"Content: {post.Content}");
                Console.WriteLine($"DateReleased: {post.DateReleased}");
                Console.WriteLine($"IdCategory : {post.IdCategory}");
                Console.WriteLine();
            }
        }
        //2. Thêm mới 1 Post
        public void Addnew()
        {

            using var dbcontext = new MyDBContext();
            var post = new PostModel();
            try
            {
                Console.WriteLine("Hãy nhập Title:  ");
                post.Title = Console.ReadLine();
                Console.WriteLine("Hãy nhập Author:  ");
                post.Author = Console.ReadLine();
                Console.WriteLine("Hãy nhập Content:  ");
                post.Content = Console.ReadLine();
                Console.WriteLine("Hãy nhập DateReleased:  ");
                post.DateReleased = Console.ReadLine();
                Console.WriteLine("Hãy nhập IdCategory:  ");
                post.IdCategory = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Thêm dữ liệu thành công");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Thêm thất bại");
                Console.ReadLine();
            }
            dbcontext.Add(post);
            dbcontext.SaveChanges();
        }
        //3. Xoá 1 Post
        public void DeletePost()
        {
            using var dbcontext = new MyDBContext();
            Console.Write("Hãy nhập Id mà bạn muốn xoá: ");
            int postId = int.Parse(Console.ReadLine());
            var post = dbcontext.posts.Find(postId);
            if (post != null)
            {
                dbcontext.posts.Remove(post);
                dbcontext.SaveChanges();
                Console.WriteLine("Post này đã được xoá : ");
            }
            else
            {
                Console.WriteLine("Post không được tìm thấy : ");
            }
        }
        //4. In ra tất cả những Category Sport
        public void CateSport()
        {
            using var dbcontext = new MyDBContext();

            var cateSport = dbcontext.posts.Where(c => c.IdCategory == 1).ToList();
            if (cateSport.Any())
            {
                Console.WriteLine("Những Post có Category là Sport :");
                foreach (var post in cateSport)
                {
                    Console.WriteLine($"ID: {post.Id}");
                    Console.WriteLine($"Title: {post.Title}");
                    Console.WriteLine($"Title: {post.Author}");
                    Console.WriteLine($"Title: {post.Content}");
                    Console.WriteLine($"Title: {post.DateReleased}");
                    Console.WriteLine($"Title: {post.IdCategory}");
                    Console.WriteLine();
                }
            }
            else
            {
                Console.WriteLine("Không có Post nào có Category là Sport.");
            }
        }
        //5. In ra tất cả bài viết được viết trước tháng 3/2023
        public void DatePost()
        {
            using var dbcontext = new MyDBContext();
            var datePost = dbcontext.posts.ToList();
            if (datePost.Any())
            {
                Console.WriteLine("Những Post được viết trước tháng 3/2023 :");
                foreach (var post in datePost)
                {
                    if (DateTime.Parse(post.DateReleased) < new DateTime(2023, 3, 1))
                    {
                        Console.WriteLine($"ID: {post.Id}");
                        Console.WriteLine($"Title: {post.Title}");
                        Console.WriteLine($"Title: {post.Author}");
                        Console.WriteLine($"Title: {post.Content}");
                        Console.WriteLine($"Title: {post.DateReleased}");
                        Console.WriteLine($"Title: {post.IdCategory}");
                        Console.WriteLine();
                    }
                }
            }
            else
            {
                Console.WriteLine("Không có bài viết nào được viết trước tháng 3/2023 :");
            }
        }

    }
}
