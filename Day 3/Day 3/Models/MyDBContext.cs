﻿using Microsoft.EntityFrameworkCore;

namespace Day_3.Models
{
    public class MyDBContext : DbContext
    {
        public DbSet<PostModel> posts { get; set; }
        public DbSet<CategoryModel> categories { get; set; }
        private const string connectionString = @"Server=127.0.0.1;Port=3306;Database=SchoolDB;Uid=root;Pwd=huyhd2001;";
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseMySQL(connectionString);
        }

    }
}
