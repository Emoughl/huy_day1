﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day_3.Models
{
    [Table("Post")]
    public class PostModel
    {

        //- ID(tang dan)
        //- Title
        //- Author
        //- IdCategory
        //- Content
        //- DateReleased
        [Key]
        public int Id { get; set; }
        [StringLength(50)]
        public string Title { get; set; }
        [StringLength(20)]
        public string Author { get; set; }
        public int IdCategory { get; set; }
        public string Content { get; set; }
        public string DateReleased { get; set; } 


    }
}
