﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day_3.Models
{

    public class MainModel
    {
        public List<CategoryModel> Category { get; set; }
        public List<PostModel> Post { get; set; }

        public MainModel()
        {
            Category = new List<CategoryModel>();
            Post = new List<PostModel>();


        }
    }
}
