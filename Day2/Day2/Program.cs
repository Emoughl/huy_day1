﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Day2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var instance = new Run();
            instance.Start();

        }
    }
}
