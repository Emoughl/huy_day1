﻿using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json.Converters;
using System.Runtime.InteropServices.ComTypes;

namespace Day2
{
    internal class Student
    {
        public string id;
        public string Name;
        public string Class;
        public int Score1;
        public int Score2;
        public int Score3;
        public int Score4;
        public Details Details;



        public List<Student> GetList()
        {
            string path = @"Student.json";
            string data = System.IO.File.ReadAllText(path);
            List<Student> item = JsonConvert.DeserializeObject<List<Student>>(data);
            return item;
        }

        //1. Lấy toàn bộ danh sách sinh viên
        public void GetAll()
        {
            var studentdts = from student in GetList()
                             select new
                             {
                                 student.id,
                                 student.Name,
                                 student.Class,
                                 student.Score1,
                                 student.Score2,
                                 student.Score3,
                                 student.Score4,
                                 student.Details
                             };
            foreach (var student in studentdts)
            {
                Console.WriteLine(" - Id sinh viên : {0} \n - Tên sinh viên : {1} \n - Lớp : {2} \n - Điểm môn học 1 : {3} \n - Điểm môn học 2 : {4} \n - Điểm môn học 3 : {5} \n - Điểm môn học 4 : {6} \n - Địa chỉ : {7} \n - Số Điện Thoại : {8}",
                    student.id, student.Name, student.Class, student.Score1, student.Score2, student.Score3, student.Score4, student.Details.Address, student.Details.Phone);
                Console.WriteLine(' ');
            }

        }
        //2 . Đếm số lượng loại sinh viên trong danh sách
        public void DemSoLuongSVLoai()
        {
            var slDiemGioi = 0;
            var slDiemKha = 0;
            var slDiemYeu = 0;
            var slDiemXuatSac = 0;
            foreach (var sl in GetList())
            {
                var diemTB = (sl.Score1 + sl.Score2 + sl.Score3 + sl.Score4) / 4;
                if (diemTB >= 9 && diemTB <= 10)
                {
                    slDiemXuatSac++;
                }
                else if (diemTB < 9 && diemTB >= 7)
                {
                    slDiemGioi++;
                }
                else if (diemTB < 7 && diemTB >= 5)
                {
                    slDiemKha++;
                }
                else { slDiemYeu++; }
            }
            Console.WriteLine(" - Số sinh viên xuất sắc là : {0} sinh viên \n - Số sinh viên giỏi là : {1} sinh viên \n - Số sinh viên khá là : {2} sinh viên \n - Số sinh viên yếu là : {3} sinh viên",
                slDiemXuatSac, slDiemGioi, slDiemKha, slDiemYeu);
        }
        //3 . Xếp loại các sinh viên
        private static string Rank(double type)
        {
            if (type < 5)
            {
                return "Yếu";
            }
            else if (type >= 5 && type < 7)
            {
                return "Khá";
            }
            else if (type >= 7 && type < 9)
            {
                return "Giỏi";
            }
            else if (type >= 9
                && type <= 10)
            {
                return "Xuất Sắc";
            }
            else
            {
                return "Null";
            }

        }
        public void XepLoaiSV()
        {
            var studentType = from student in GetList()
                              select new
                              {
                                  Name = student.Name,
                                  TypeScore = (student.Score1 + student.Score2 + student.Score3 + student.Score4) / 4, // Tổng điểm trung bình
                                  Type = Rank((student.Score1 + student.Score2 + student.Score3 + student.Score4) / 4) // Xếp loại dựa vào điểm trung bình đã thiết lập Rank
                              };
            foreach (var student in studentType)
            {
                Console.WriteLine(" - Tên sinh viên : {0} \n - Điểm Trung Bình : {1} \n - Xếp Loại : {2}", student.Name, student.TypeScore, student.Type);
                Console.WriteLine(' ');
            }
        }
        //4 . In ra các sinh viên yếu bị cảnh cáo
        public void SVYeu()
        {
            var studentType = from student in GetList()
                              where (student.Score1 + student.Score2 + student.Score3 + student.Score4) / 4 < 5 // Loại yếu dtb < 5
                              select new
                              {
                                  Name = student.Name,
                                  TypeScore = (student.Score1 + student.Score2 + student.Score3 + student.Score4) / 4,
                                  Type = Rank((student.Score1 + student.Score2 + student.Score3 + student.Score4) / 4)
                              };
            Console.WriteLine("Các sinh viên yếu bị cảnh cáo: ");
            foreach (var student in studentType)
            {

                Console.WriteLine("- Tên : {0} \n - Điểm Trung Bình : {1} \n - Xếp Loại : {2}", student.Name, student.TypeScore, student.Type);
                Console.WriteLine(' ');
            }
        }
        //5 . Tìm kiếm sinh viên dựa tên, in ra thông tin sinh viên đó
        public void ThongTinSV()
        {

            Console.WriteLine("Hãy nhập tên sinh viên : ");
            string name = Console.ReadLine();
            var selectName = from student in GetList()
                             where student.Name.ToLower() == name.ToLower() // Nhận diện ký tự 
                             select student;


            foreach (var student in selectName)
            {
                Console.WriteLine("- Id sinh viên : " + student.id);
                Console.WriteLine("- Tên sinh viên: " + student.Name);
                Console.WriteLine("- Lớp : " + student.Class);
                Console.WriteLine("- Điểm môn học 1: " + student.Score1);
                Console.WriteLine("- Điểm môn học 2: " + student.Score2);
                Console.WriteLine("- Điểm môn học 3: " + student.Score3);
                Console.WriteLine("- Điểm môn học 4: " + student.Score4);
                Console.WriteLine("- Đia chỉ: " + student.Details.Address);
                Console.WriteLine("- Số điện thoại: " + student.Details.Phone);
                Console.WriteLine(' ');
            }


            if (!selectName.Any())
            {
                Console.WriteLine("Không tìm thấy sinh viên này , hãy thử lại");
                ThongTinSV();
            }
        }
        //6 . Thay đổi thông tin sinh viên
        public void SuaThongTinSV()
        {
            Console.WriteLine("Hãy nhập ID sinh viên cần sửa : ");
            var StudentList = GetList();
            string ID = Console.ReadLine();
            var selectStudent = from student in StudentList
                                where student.id.ToLower() == ID.ToLower()
                                select student;
            if (selectStudent.Any())
            {
                Console.WriteLine("Hãy nhập thông tin mới cho sinh viên :");
                Console.WriteLine("Id sinh viên:");
                selectStudent.First().id = Console.ReadLine();

                Console.WriteLine("Tên sinh viên:");
                selectStudent.First().Name = Console.ReadLine();

                Console.WriteLine("Lớp:");
                selectStudent.First().Class = Console.ReadLine();

                Console.WriteLine("Điểm môn học 1:");
                selectStudent.First().Score1 = int.Parse(Console.ReadLine());

                Console.WriteLine("Điểm môn học 2:");
                selectStudent.First().Score2 = int.Parse(Console.ReadLine());

                Console.WriteLine("Điểm môn học 3:");
                selectStudent.First().Score3 = int.Parse(Console.ReadLine());

                Console.WriteLine("Điểm môn học 4:");
                selectStudent.First().Score4 = int.Parse(Console.ReadLine());

                Console.WriteLine("Địa chỉ:");
                selectStudent.First().Details.Address = Console.ReadLine();

                Console.WriteLine("Số Điện Thoại:");
                selectStudent.First().Details.Phone = Console.ReadLine();

                JsonSerializer serializer = new JsonSerializer();
                serializer.Converters.Add(new JavaScriptDateTimeConverter());
                serializer.NullValueHandling = NullValueHandling.Ignore;

                string Filename = @"Student.json";
                FileStream stream = null;
                stream = new FileStream(Filename, FileMode.Create);

                using (StreamWriter writer = new StreamWriter(stream))
                {

                    writer.Write(JsonConvert.SerializeObject(StudentList));
                    writer.Close();

                }
                stream.Dispose(); 



                Console.WriteLine("Thông tin sinh viên đã được cập nhật:");
                Console.WriteLine();
                GetAll();
            }
            else
            {
                Console.WriteLine("Không tìm thấy sinh viên này, hãy thử lại");
            }

        }
        //7 Xoá sinh viên tại vị trí chỉ định: nếu không tìm đc sinh viên trả message không tìm đc sinh viên
        public void XoaSinhVien()
        {
            Console.WriteLine("Hãy nhập vị trí của sinh viên cần xoá : ");
            List<Student> studentList = GetList();
            int student = Convert.ToInt32(Console.ReadLine());
            if (student >= 0 && student < studentList.Count)
            {
                studentList.RemoveAt(student);

                string path = @"Student.json";
                string json = JsonConvert.SerializeObject(studentList);
                System.IO.File.WriteAllText(path, json);

                Console.WriteLine("Đã xoá sinh viên tại vị trí {0}", student);
            }
            else
            {
                Console.WriteLine("Không tìm thấy sinh viên tại vị trí {0}", student);
            }

        }

    }
}
