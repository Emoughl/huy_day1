﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Day2
{
    internal class Run
    {
        public void Start()
        {
            
            Console.OutputEncoding = Encoding.UTF8; 
            string path = @"Student.json";
            string data = System.IO.File.ReadAllText(path);
            List<Student> item = JsonConvert.DeserializeObject<List<Student>>(data);
            Student std = new Student();
            do
            {
                
                Console.WriteLine("Hiện có các lựa chọn sau : ");
                Console.WriteLine("1. Lấy toàn bộ danh sách sinh vien");
                Console.WriteLine("2. Đếm số lượng loại sinh viên trong danh sách");
                Console.WriteLine("3. Xếp loại các sinh viên");
                Console.WriteLine("4. In ra các sinh viên yếu bị cảnh cáo");
                Console.WriteLine("5. Tìm kiếm sinh viên dựa tên, in ra thông tin sinh viên đó");
                Console.WriteLine("6. Thay đổi thông tin sinh viên");
                Console.WriteLine("7. Xoá sinh viên tại vị trí chỉ định: nếu không tìm đc sinh viên trả message không tìm đc sinh viên");
                Console.WriteLine("8. Thoát");
                Console.Write("Hãy đưa ra lựa chọn của bạn: ");
                string choice = Console.ReadLine();
                Console.Clear();
                switch (choice)
                {
                    case "1":
                        std.GetAll();
                        Console.ReadLine();
                        Console.Clear();
                 
                        break;
                    case "2":
                        std.DemSoLuongSVLoai();
                        Console.ReadLine();
                        Console.Clear();
                       
                        break;
                    case "3":
                        std.XepLoaiSV();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "4":
                        std.SVYeu();
                        Console.ReadLine();
                        Console.Clear();
                      
                        break;
                    case "5":
                        std.ThongTinSV();
                        Console.ReadLine();
                        Console.Clear();
                    
                        break;
                    case "6":
                        std.SuaThongTinSV();
                        Console.ReadLine();
                        Console.Clear();
                       
                        break;
                    case "7":
                        std.XoaSinhVien();
                        Console.WriteLine("Đang Chuyển tới danh sach sinh viên .....");
                        Thread.Sleep(5000);
                        std.GetAll();
                        Console.ReadLine();
                        Console.Clear();
                       
                        break;
                    case "8":
                        Console.WriteLine("Đang Thoát .....");
                        System.Environment.Exit(1);

                        break;
                    default:
                        Console.WriteLine("Có gì đó sai sai . Bạn nhập đúng số chứ ?");
                        Console.ReadLine();
                        Console.Clear();
                     
                        break;
                }

                Console.ReadLine();
                Start();
            } while (false);
        }



    }
}
