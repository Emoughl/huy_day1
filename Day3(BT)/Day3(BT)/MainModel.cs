﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day3_BT_.Model
{
    public class MainModel
    {
        public List<Category> Category { get; set; }
        public List<Post> Post { get; set; }

        public MainModel()
        {
            Category = new List<Category>();
            Post = new List<Post>();
        }
    }
}
