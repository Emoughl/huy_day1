﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day3_BT_.Model
{
    [Table("Post")]
    public class Post
    {
        //- ID(tang dan)
        //- Title
        //- Author
        //- IdCategory
        //- Content
        //- DateReleased
        [Key]
        public int Id { get; set; }
        [StringLength(50)]
        public string Title { get; set; }
        [StringLength(20)]
        public string Author { get; set; }
        public int IdCategory { get; set; }
        [ForeignKey("IdCategory")]
        public string Content { get; set; }
        public string DateReleased { get; set; }

        public ICollection<Category> Categories{ get; set;}
    }
}
