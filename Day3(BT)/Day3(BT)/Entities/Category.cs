﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day3_BT_.Model
{
    [Table("Categories")]
    public class Category
    {
        //- ID(tang dan)
        //- Name
        [Key]
        public int Id { get; set; }
        [StringLength(50)]
        public string Name { get; set; }

        public ICollection<Post> Posts { get; set;}
    }
}
