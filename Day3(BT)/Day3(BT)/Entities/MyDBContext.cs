﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day3_BT_.Model
{
    public class MyDBContext : DbContext
    {
        public DbSet<Post> posts { get; set; }
        public DbSet<Category> categories { get; set; }
        private const string connectionString = @"Server=PC-HUY\MSSQLSERVER1;Database=SchoolDB;Trusted_Connection=True;Encrypt=False;User id=sa; password=huyhd2001";
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(connectionString);

        }


    }
}
