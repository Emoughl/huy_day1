﻿using Day3_BT_.Model;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ToolTip;

namespace Day3_BT_
{
    class Program
    {

        //1. Sử dụng EF generate code-first ra db
        //Tạo dữ liệu database
        static void CreateDatabase()
        {
            using var dbcontext = new MyDBContext();
            string dbname = dbcontext.Database.GetDbConnection().Database;
            var kq = dbcontext.Database.EnsureCreated();
            if (kq)
            {
                Console.WriteLine($"Tao db {dbname} thanh cong");
            }
            else
            {
                Console.WriteLine($"Tao That bai {dbname}");
            }
        }
        //Xoá dữ liệu database
        static void DropDatabase()
        {
            using var dbcontext = new MyDBContext();
            string dbname = dbcontext.Database.GetDbConnection().Database;
            var kq = dbcontext.Database.EnsureDeleted();
            if (kq)
            {
                Console.WriteLine($"Xoa db {dbname} thanh cong");
            }
            else
            {
                Console.WriteLine($"Xoa That bai {dbname}");
            }
        }
        //Xuất dữ liệu trong file Json
        static void Start()
        {
            Console.OutputEncoding = Encoding.UTF8;
            Choose choose = new Choose();
            do
            {
                Console.WriteLine("Hiện có các lựa chọn sau : ");
                Console.WriteLine("0. Thêm dữ liệu vào database");
                Console.WriteLine("1. Xuất dữ liệu Post, Category");
                Console.WriteLine("2. Thêm mới 1 Post");
                Console.WriteLine("3. Xoá 1 Post");
                Console.WriteLine("4. In ra tất cả những Category Sports");
                Console.WriteLine("5. In ra tất cả bài viết được viết trước tháng 3/2023");
                Console.WriteLine("6. Thoát");
                Console.Write("Hãy đưa ra lựa chọn của bạn: ");
                string choice = Console.ReadLine();
                Console.Clear();
                switch (choice)
                {
                    case "0":
                        choose.InsertDatabase();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "1":
                        choose.GetAll();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "2":
                        choose.Addnew();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "3":
                        choose.DeletePost();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "4":
                        choose.CateSport();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "5":
                        choose.DatePost();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "6":
                        Console.WriteLine("Đang Thoát .....");
                        System.Environment.Exit(1);
                        break;
                    default:
                        Console.WriteLine("Có gì đó sai sai . Bạn nhập đúng số chứ ?");
                        Console.ReadLine();
                        Console.Clear();
                        break;
                }
                Console.ReadLine();
                Start();
            } while (false);
        }
        static void Main(string[] args)
        {

            //DropDatabase();
            //CreateDatabase();
            Start();
            Console.ReadLine();

        }

    }
}
