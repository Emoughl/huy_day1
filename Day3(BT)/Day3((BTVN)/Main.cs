﻿using Day3__BTVN_.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day3__BTVN_
{
    public class Main
    {
        public List<PersonalData> PersonalDatas { get; set; }
        public List<Student> Students { get; set; }
        public List<Subject> Subjects { get; set; }
        public List<TuitionSubject> TuitionSubjects { get; set; }

        public Main()
        {
            PersonalDatas = new List<PersonalData>();
            Students = new List<Student>();
            Subjects = new List<Subject>();
            TuitionSubjects = new List<TuitionSubject>();
        }
    }
}
