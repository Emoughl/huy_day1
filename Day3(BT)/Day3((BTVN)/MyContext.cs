﻿using Day3__BTVN_.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day3__BTVN_
{
    public class MyContext : DbContext
    {
        public virtual DbSet<Student> Students { get; set; }
        public virtual DbSet<PersonalData> PersonalDatas { get; set; }
        public virtual DbSet<Subject> Subjects { get; set; }
        public virtual DbSet<TuitionSubject> TuitionSubjects { get; set; }

        private const string connectionString = @"Server=PC-HUY\MSSQLSERVER1;Database=BTVN;Trusted_Connection=True;Encrypt=False;User id=sa; password=huyhd2001";
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(connectionString);

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            {
                modelBuilder.Entity<TuitionSubject>()
                    .HasKey(ts => new { ts.StudentId, ts.SubjectId, ts.Semester });

                modelBuilder.Entity<TuitionSubject>()
                    .HasOne(ts => ts.Student)
                    .WithMany(s => s.TuitionSubjects)
                    .HasForeignKey(ts => ts.StudentId);

                modelBuilder.Entity<TuitionSubject>()
                    .HasOne(ts => ts.Subject)
                    .WithMany(s => s.TuitionSubjects)
                    .HasForeignKey(ts => ts.SubjectId);
            }

        }


    }
}
