﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day3__BTVN_.Model
{
    public class PersonalData
    {
        [Key]
        [MaxLength(12)]
        public string IdentityCard { get; set; }

        [MaxLength(10)]
        public string Phone { get; set; }

        [MaxLength(50)]
        public string Address { get; set; }

        public DateTime Birthday { get; set; }

        public virtual ICollection<Student> Students { get; set; }

    }
}
