﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day3__BTVN_.Model
{
    public class Student
    {
        [Key]
        public int Id { get; set; }


        [Required]
        [MaxLength(10)]
        public string StudentCode { get; set; }  

        [MaxLength(50)]
        public string Name { get; set; }

        [MaxLength(50)]
        public string Class { get; set; }

        
        [Required]
        [MaxLength(12)]
        public string PersonalDataId { get; set; }
        [ForeignKey("PersonalDataId")]

        [Required]
        public bool IsDeleted { get; set; }

        public PersonalData PersonalData { get; set; }
        public ICollection<TuitionSubject> TuitionSubjects { get; set; }

    }
}
