﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day3__BTVN_.Model
{

    public class TuitionSubject
    {
     
        public int StudentId { get; set; }
        [ForeignKey("StudentId")]
        [Required]

        public int SubjectId { get; set; }
        [ForeignKey("SubjectId")]
        [Required]
        [Key]
        public string Semester { get; set; }

        public float? TotalFee { get; set; }

        public Student Student { get; set; }
        public Subject Subject { get; set; }
    }

}
