﻿using Day3__BTVN_.Model;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ToolTip;

namespace Day3__BTVN_
{
    public class Program
    {
        //Xoá dữ liệu database
        static void DropDatabase()
        {
            using var dbcontext = new MyContext();
            string dbname = dbcontext.Database.GetDbConnection().Database;
            var kq = dbcontext.Database.EnsureDeleted();
            if (kq)
            {
                Console.WriteLine($"Xoa db {dbname} thanh cong");
            }
            else
            {
                Console.WriteLine($"Xoa That bai {dbname}");
            }
        }
        //Tạo File json
        static void AddData()
        {
            var mains = new Main
            {
                PersonalDatas = new List<PersonalData>
                {
                    new PersonalData {IdentityCard = "1911064578", Phone = "0937457715", Address = "NT", Birthday = new DateTime(2000, 8, 22) },
                    new PersonalData {IdentityCard = "1911062017", Phone = "0931971560", Address = "SG", Birthday = new DateTime(2001, 5, 28) },
                    new PersonalData {IdentityCard = "1911069854", Phone = "0934817469", Address = "NT", Birthday = new DateTime(2001, 3, 18) }
                },
                Students = new List<Student>
                {
                    new Student { Id = 0,StudentCode = "ST001", Name = "Khoi", Class = "A1", PersonalDataId = "1911064578", IsDeleted = false },
                    new Student { Id = 0,StudentCode = "ST002", Name = "Huy", Class = "A2", PersonalDataId = "1911062017", IsDeleted = false },
                    new Student { Id = 0,StudentCode = "ST003", Name = "Khanh", Class = "A3", PersonalDataId = "1911069854", IsDeleted = false }
                },
                Subjects = new List<Subject>
                {
                    new Subject { Id = 0, Name = "Math" },
                    new Subject { Id = 0, Name = "Literature" },
                    new Subject { Id = 0, Name = "English" }
                },
                TuitionSubjects = new List<TuitionSubject>
            {
            new TuitionSubject { StudentId = 1, SubjectId = 1,Semester ="1/2019" , TotalFee = 25000000 },
            new TuitionSubject { StudentId = 1, SubjectId = 2,Semester = "3/2019", TotalFee = 15000000 },
            new TuitionSubject { StudentId = 1, SubjectId = 3,Semester = "4/2019", TotalFee = 15000000 },
            new TuitionSubject { StudentId = 2, SubjectId = 1,Semester = "1/2019", TotalFee = 10500000 },
            new TuitionSubject { StudentId = 2, SubjectId = 2,Semester = "2/2019", TotalFee = 18500000 },
            new TuitionSubject { StudentId = 2, SubjectId = 3,Semester = "4/2019", TotalFee = 16500000 },
            new TuitionSubject { StudentId = 3, SubjectId = 1,Semester = "1/2019", TotalFee = 15500000 },
            new TuitionSubject { StudentId = 3, SubjectId = 2,Semester = "2/2019", TotalFee = 20000000 },
            new TuitionSubject { StudentId = 3, SubjectId = 3,Semester = "3/2019", TotalFee = 25000000 }
            }
            };

            string json = JsonConvert.SerializeObject(mains);
            string filePath = "databt.json";
            File.WriteAllText(filePath, json);

        }
        static void Start()
        {
            Console.OutputEncoding = Encoding.UTF8;
            Choose choose = new Choose();
            do
            {
                Console.WriteLine("Hiện có các lựa chọn sau : ");
                //Đọc dữ liệu từ file Json, kiểm tra xem đã tồn tại tất cả sinh viên trong danh sách đó hay chưa, chưa thì tiến hành add những sinh viên đó (1,2)
                Console.WriteLine("1. Xem toàn bộ sinh viên trong danh sách");
                Console.WriteLine("2. Chưa có ? hãy tiến hành thêm những sinh viên đó");
                Console.WriteLine("3. Thêm một bạn sinh viên");
                Console.WriteLine("4. Tìm kiếm sinh viên dựa tên, in ra thông tin sinh viên đó");
                Console.WriteLine("5. Cập nhật thông tin sinh viên");
                Console.WriteLine("6.  Xoá sinh viên tại vị trí chỉ định");
                Console.WriteLine("7.  Thêm 1 hóa đơn TuitionSubjects của 1 bạn học sinh cho 1 môn học");
                Console.WriteLine("8.  Tính tổng số học phí của từng bạn phải đóng");
                Console.WriteLine("9.  Tính học phí của mỗi người trên mỗi môn học");
                Console.WriteLine("10.  Thống kê học phí theo độ tuổi");
                Console.WriteLine("11.  Tìm môn học có nhiều lần học nhất");
                Console.WriteLine("0. Thoát");
                Console.Write("Hãy đưa ra lựa chọn của bạn: ");
                string choice = Console.ReadLine();
                Console.Clear();
                switch (choice)
                {
                    case "1":
                        choose.ListStudent();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "2":
                        choose.MakeData();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "3":
                        choose.AddStudent();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "4":
                        choose.SearchStudent();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "5":
                        choose.UpdateStudent();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "6":
                        choose.DeleteStudent();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "7":
                        choose.TuitionSubject();
                        choose.AddTuitionSubject();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "8":
                        choose.CalculateTotalFee();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "9":
                        choose.CalculateSubjectFee();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "10":
                        choose.TuitionAge();
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case "11":
                        choose.MostSubject();
                        Console.ReadLine();
                        Console.Clear();
                        break;

                    case "12":
                        Console.WriteLine("Đang Thoát .....");
                        System.Environment.Exit(1);
                        break;

                    default:
                        Console.WriteLine("Có gì đó sai sai . Bạn nhập đúng số chứ ?");
                        Console.ReadLine();
                        Console.Clear();
                        break;
                }
                Console.ReadLine();
                Start();
            } while (false);
        }
        static void Main(string[] args)
        {

            //DropDatabase();
            //AddData();
            Start();
            Console.ReadLine();

        }
    }
}
