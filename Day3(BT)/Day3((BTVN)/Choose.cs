﻿using Day3__BTVN_.Model;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day3__BTVN_
{
    public class Choose
    {
        //Xem toàn bộ sinh viên trong danh sách
        public void ListStudent()
        {
            using (var db = new MyContext())
            {
                var students = db.Students.Include(s => s.PersonalData).Where(s => !s.IsDeleted).ToList();
                Console.WriteLine("==========Danh sách sinh viên==========");
                Console.WriteLine("");
                if (students.Count == 0)
                {
                    Console.WriteLine("Không tìm thấy sinh viên nào , hãy thêm dữ liệu tại lựa chọn 2");
                }
                else
                {
                    foreach (var student in students)
                    {
                        Console.WriteLine($"ID Sinh Viên: {student.StudentCode}");
                        Console.WriteLine($"Tên: {student.Name}");
                        Console.WriteLine($"Lớp: {student.Class}");
                        Console.WriteLine($"Số Căn Cước : {student.PersonalData.IdentityCard}");
                        Console.WriteLine($"Số Điện Thoại: {student.PersonalData.Phone}");
                        Console.WriteLine($"Địa Chỉ: {student.PersonalData.Address}");
                        Console.WriteLine($"Ngày sinh: {student.PersonalData.Birthday.ToString("dd/MM/yyyy")}");
                        Console.WriteLine("");
                    }
                }

            }
        }
        //Thêm dữ liệu vào data
        public void MakeData()
        {
            string filePath = "databt.json";
            if (!File.Exists(filePath))
            {
                Console.WriteLine("Thêm thất bại ");
                return;
            }
            else
            {
                string json = File.ReadAllText(filePath);
                Main Mains = JsonConvert.DeserializeObject<Main>(json);
                using var dbcontext = new MyContext();

                foreach (PersonalData personalData in Mains.PersonalDatas)
                {
                    string query = $"INSERT INTO personalData VALUES ({personalData})";
                    dbcontext.PersonalDatas.Add(personalData);
                }
                foreach (Student student in Mains.Students)
                {
                    string query = $"INSERT INTO student VALUES ({student})";
                    dbcontext.Students.Add(student);
                }
                foreach (Subject subject in Mains.Subjects)
                {
                    string query = $"INSERT INTO subject VALUES ({subject})";
                    dbcontext.Subjects.Add(subject);
                }
                dbcontext.SaveChanges();
                foreach (TuitionSubject tuitionSubject in Mains.TuitionSubjects)
                {
                    string query = $"INSERT INTO tuitionSubject VALUES ({tuitionSubject})";
                    dbcontext.TuitionSubjects.Add(tuitionSubject);
                }
                dbcontext.SaveChanges();
                Console.WriteLine("Thêm thành công ");
            }
        }
        //Thêm 1 sinh viên mới
        public void AddStudent()
        {
            using var dbcontext = new MyContext();
            var student = new Student();
            var data = new PersonalData();
            try
            {
                Console.WriteLine("Hãy nhập code sinh viên :  ");
                student.StudentCode = Console.ReadLine();
                Console.WriteLine("Hãy nhập tên sinh viên:  ");
                student.Name = Console.ReadLine();
                Console.WriteLine("Hãy nhập lớp:  ");
                student.Class = Console.ReadLine();
                student.IsDeleted = Convert.ToBoolean(false);
                Console.WriteLine("Hãy nhập số chứng minh : ");
                student.PersonalDataId = data.IdentityCard = Console.ReadLine();
                Console.WriteLine("Hãy nhập số điện thoại : ");
                data.Phone = Console.ReadLine();
                Console.WriteLine("Hãy nhập địa chỉ : ");
                data.Address = Console.ReadLine();
                Console.WriteLine("Hãy nhập ngày sinh (theo định dạng yyyy-mm-dd):");
                data.Birthday = DateTime.Parse(Console.ReadLine());
                student.PersonalData = data;
                Console.WriteLine("Thêm dữ liệu thành công");
                dbcontext.Students.Add(student);
                dbcontext.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Thêm thất bại");
                Console.ReadLine();
                return;
            }

        }
        //Tìm kiếm sinh viên dựa tên, in ra thông tin sinh viên đó
        public void SearchStudent()
        {
            using var dbcontext = new MyContext();
            try
            {
                Console.WriteLine("Hãy nhập tên sinh viên cần tìm kiếm:");
                string name = Console.ReadLine();
                var students = dbcontext.Students.Include(s => s.PersonalData)
                                                 .Where(s => s.Name == name && s.IsDeleted == false)
                                                 .ToList();
                if (students.Count > 0)
                {
                    Console.WriteLine($"Có {students.Count} sinh viên có tên {name}:");
                    Console.WriteLine("======Thông Tin Sinh Viên======");
                    foreach (var student in students)
                    {

                        Console.WriteLine($"Id: {student.StudentCode}");
                        Console.WriteLine($"Name: {student.Name}");
                        Console.WriteLine($"Lớp: {student.Class}");
                        Console.WriteLine($"Số chứng minh: {student.PersonalData.IdentityCard}");
                        Console.WriteLine($"Số điện thoại: {student.PersonalData.Phone}");
                        Console.WriteLine($"Địa chỉ: {student.PersonalData.Address}");
                        Console.WriteLine($"Ngày sinh: {student.PersonalData.Birthday.ToString("yyyy-MM-dd")}");
                        Console.WriteLine();
                    }
                }
                else
                {
                    Console.WriteLine($"Không tìm thấy sinh viên có tên {name}.");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Tìm kiếm thất bại");
                Console.ReadLine();
                return;
            }
        }
        //Cập nhật thông tin sinh viên.(giá trị phải thay đổi trên database)
        public void UpdateStudent()
        {
            using var dbcontext = new MyContext();
            try
            {
                Console.WriteLine("Hãy nhập Id của sinh viên cần cập nhật:");
                int id = int.Parse(Console.ReadLine());

                var student = dbcontext.Students.Include(s => s.PersonalData)
                                                .FirstOrDefault(s => s.Id == id && s.IsDeleted == false);
                if (student != null)
                {
                    Console.WriteLine("Hãy nhập thông tin mới của sinh viên:");
                    Console.Write("StudentCode: ");
                    string studentcode = Console.ReadLine();
                    Console.Write("Tên: ");
                    string name = Console.ReadLine();
                    Console.Write("Lớp: ");
                    string className = Console.ReadLine();
                    Console.Write("Số chứng minh: ");
                    string identityCard = Console.ReadLine();
                    Console.Write("Số điện thoại: ");
                    string phone = Console.ReadLine();
                    Console.Write("Địa chỉ: ");
                    string address = Console.ReadLine();
                    Console.Write("Ngày sinh (yyyy-MM-dd): ");
                    DateTime birthday = DateTime.Parse(Console.ReadLine());

                    student.StudentCode = studentcode;
                    student.Name = name;
                    student.Class = className;
                    student.PersonalData.IdentityCard = identityCard;
                    student.PersonalData.Phone = phone;
                    student.PersonalData.Address = address;
                    student.PersonalData.Birthday = birthday;

                    dbcontext.SaveChanges();

                    Console.WriteLine($"Đã cập nhật thông tin sinh viên {name}.");
                }
                else
                {
                    Console.WriteLine($"Không tìm thấy sinh viên có Id là : {id}.");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Cập nhật thông tin thất bại");
                Console.ReadLine();
                return;
            }
        }
        //Xoá sinh viên tại vị trí chỉ định, khi chọn xóa hỏi người dùng có muốn xóa người này không, nếu có mới tiến hành xóa.
        public void DeleteStudent()
        {
            using var dbcontext = new MyContext();
            try
            {
                Console.WriteLine("Hãy nhập Id của sinh viên cần xóa:");
                int id = int.Parse(Console.ReadLine());

                var student = dbcontext.Students.FirstOrDefault(s => s.Id == id && s.IsDeleted == false);
                if (student != null)
                {
                    Console.WriteLine($"Bạn có chắc muốn xóa sinh viên {student.Name}? (Y/N)");
                    var confirm = Console.ReadLine().ToLower();
                    if (confirm == "y")
                    {
                        student.IsDeleted = true;
                        dbcontext.SaveChanges();
                        Console.WriteLine($"Đã xóa sinh viên {student.Name}.");
                    }
                    else
                    {
                        Console.WriteLine($"Không xóa sinh viên {student.Name}.");
                    }
                }
                else
                {
                    Console.WriteLine($"Không tìm thấy sinh viên có Id {id}.");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Xoá sinh viên thất bại");
                Console.ReadLine();
                return;
            }
        }
        //Thêm 1 hóa đơn TuitionSubjects của 1 bạn học sinh cho 1 môn học
        public void TuitionSubject()
        {
            using (var db = new MyContext())
            {
                var tuitionSubjects = db.TuitionSubjects.Include(s => s.Subject).ToList();
                Console.WriteLine("Những hoá đơn môn học hiện có : ");
                Console.WriteLine("");

                foreach (var tuitionSubject in tuitionSubjects)
                {
                    Console.WriteLine($"StudentId: {tuitionSubject.StudentId}");
                    Console.WriteLine($"SubjectId: {tuitionSubject.SubjectId}");
                    Console.WriteLine($"Semeter: {tuitionSubject.Semester}");
                    Console.WriteLine($"TotalFree: {tuitionSubject.TotalFee}");
                    Console.WriteLine("");
                }

            }
        }
        public void AddTuitionSubject()
        {
            using (var db = new MyContext())
            {
                Console.WriteLine("Thêm hóa đơn học phí cho sinh viên");
                Console.WriteLine("");
                Console.Write("Nhập mã số sinh viên: ");
                int studentId = int.Parse(Console.ReadLine());
                var student = db.Students.FirstOrDefault(s => s.Id == studentId && !s.IsDeleted);
                if (student == null)
                {
                    Console.WriteLine($"Không tìm thấy sinh viên có mã số {studentId}");
                    return;
                }

                Console.Write("Nhập tên môn học: ");
                string subjectName = Console.ReadLine();

                var subject = db.Subjects.FirstOrDefault(s => s.Name.ToLower() == subjectName.ToLower());
                if (subject == null)
                {
                    Console.WriteLine($"Không tìm thấy môn học có tên {subjectName}");
                    return;
                }

                Console.Write("Nhập giá môn học: ");
                float totalFee;
                while (!float.TryParse(Console.ReadLine(), out totalFee))
                {
                    Console.WriteLine("Vui lòng nhập giá tiền là số:");
                }

                Console.Write("Nhập kỳ học: ");
                string semester = Console.ReadLine();
                while (string.IsNullOrEmpty(semester) || db.TuitionSubjects.Any(ts => ts.StudentId == student.Id && ts.SubjectId == subject.Id && ts.Semester.ToLower() == semester.ToLower()))
                {
                    Console.WriteLine($"Sinh viên {student.Name} đã đăng ký học môn {subject.Name} cho kỳ học {semester}. Vui lòng chọn kỳ học khác.");
                    Console.Write("Nhập kỳ học: ");
                    semester = Console.ReadLine();
                }

                var tuitionSubject = new TuitionSubject
                {
                    StudentId = student.Id,
                    SubjectId = subject.Id,
                    TotalFee = totalFee,
                    Semester = semester
                };

                db.TuitionSubjects.Add(tuitionSubject);
                db.SaveChanges();

                Console.WriteLine($"Đã thêm hóa đơn học phí cho sinh viên {student.Name} vào môn học {subjectName} thành công.");
            }
        }
        //Tính tổng số học phí của từng bạn phải đóng
        public void CalculateTotalFee()
        {
            using (var db = new MyContext())
            {
                Console.WriteLine("Tính tổng số học phí của từng sinh viên");
                Console.WriteLine("");
                var students = db.Students.Include(s => s.TuitionSubjects).ToList();
                foreach (var student in students)
                {
                    float? totalFee = 0;
                    foreach (var tuitionSubject in student.TuitionSubjects)
                    {
                        totalFee += tuitionSubject.TotalFee;
                    }
                    Console.WriteLine($"Sinh viên {student.Name} có mã số sinh viên là : {student.StudentCode} phải đóng tổng số học phí là {totalFee}.");
                }
            }
        }
        //Tính học phí của mỗi người trên mỗi môn học
        public void CalculateSubjectFee()
        {
            using (var db = new MyContext())
            {
                Console.WriteLine("Tính học phí của mỗi người trên mỗi môn học");
                Console.WriteLine("");
                var tuitionSubjects = db.TuitionSubjects.Include(s => s.Student).Include(s => s.Subject).ToList();

                // Lấy danh sách các môn học
                var subjects = tuitionSubjects.Select(ts => ts.Subject).Distinct();

                foreach (var subject in subjects)
                {
                    Console.WriteLine($"Môn học {subject.Name}:");
                    Console.WriteLine("---------------------");

                    // Lấy danh sách các sinh viên đăng ký môn học
                    var students = tuitionSubjects.Where(ts => ts.SubjectId == subject.Id).Select(ts => ts.Student).Distinct();

                    foreach (var student in students)
                    {
                        // Tính tổng số tiền học phí của sinh viên trên môn học
                        float? totalFee = tuitionSubjects.Where(ts => ts.StudentId == student.Id && ts.SubjectId == subject.Id).Sum(ts => ts.TotalFee);

                        Console.WriteLine($"Sinh viên {student.Name} (mã số sinh viên là : {student.StudentCode} phải đóng học phí là {totalFee} đồng.");
                    }

                    Console.WriteLine("");
                }
            }
        }
        //Thống kê học phí theo độ tuổi. 
        public void TuitionAge()
        {
            using (var db = new MyContext())
            {
                var tuitionSubjects = db.TuitionSubjects.Include(ts => ts.Student)
                                                          .ThenInclude(s => s.PersonalData)
                                                          .ToList();

                var ageGroups = new List<(string Name, int MinAge, int MaxAge)>
                {
                    ("Dưới 15 tuổi", 0, 14),
                    ("Từ 15 đến 20 tuổi", 15, 20),
                    ("Từ 20 đến 30 tuổi", 20, 30),
                    ("Trên 30 tuổi", 31, int.MaxValue)
                };

                var totalFeesByAgeGroup = new Dictionary<string, float>();

                foreach (var ageGroup in ageGroups)
                {
                    var today = DateTime.Today.Year;
                    DateTime current_year = DateTime.Now;
                    var studentsInAgeGroup = db.Students.Include(s => s.PersonalData)
                                                        .Where(s => today - s.PersonalData.Birthday.Year >= ageGroup.MinAge && today - s.PersonalData.Birthday.Year <= ageGroup.MaxAge)
                                                        .ToList();

                    var totalFees = tuitionSubjects.Where(ts => studentsInAgeGroup.Contains(ts.Student))
                                                   .Sum(ts => ts.TotalFee);

                    totalFeesByAgeGroup.Add(ageGroup.Name, (float)totalFees);
                }

                Console.WriteLine("Tổng học phí theo độ tuổi:");
                Console.WriteLine("");

                foreach (var item in totalFeesByAgeGroup)
                {
                    Console.WriteLine($"{item.Key}: {item.Value}");
                }
            }

        }
        //Tìm môn học có nhiều lần học nhất
        public void MostSubject()
        {
            using (var db = new MyContext())
            {
                var subjects = db.TuitionSubjects
                                .GroupBy(t => t.Subject.Name)
                                .Select(g => new
                                {
                                    SubjectName = g.Key,
                                    Count = g.Count()
                                })
                                .OrderByDescending(x => x.Count)
                                .FirstOrDefault();

                Console.WriteLine($"Môn học có số lần học nhiều nhất là :  {subjects.SubjectName}");
            }

        }

    }
}

